# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=invalid-name
# pylint: disable=trailing-newlines
# pylint: disable=trailing-whitespace
# pylint: disable=missing-final-newline
# pylint: disable=W0718

import os
import sys
from utils import loadCookies, getReward, isClaimed, claimReward, parseReward, logging

COOKIE = os.environ.get('COOKIE')


def main():
    logging("Starting...")
    if not COOKIE:
        print("There's no COOKIE set, bot can't run without that. Quiting with error...",
              file=sys.stderr)
        quit(1)
    multicookies = []
    for cookie in COOKIE.split("\n"):
        multicookies.append(loadCookies(cookie))
    if len(multicookies) >= 2:
        print(
            f"Multi account detected, logging in with {multicookies} accounts")
    acccount = 0
    for cookies in multicookies:
        acccount += 1
        counter_str = f"[{acccount}] " if len(multicookies) >= 2 else ""
        logging(f"{counter_str}Connecting to server...")
        try:
            reward = getReward(cookies)
            check, day = isClaimed(cookies)
        except Exception as e:
            logging(f"{counter_str}Unexpected Error [1]: {repr(e)}", 3)
            continue
        if str(check).lower() == "not logged in":
            logging(f"{counter_str}Failed to logged in, maybe wrong cookie?", 3)
            continue
        if not check:
            logging(f"{counter_str}Claiming daily...")
            try:
                c = claimReward(cookies)
            except Exception as e:
                logging(f"{counter_str}Unexpected Error [2]: {repr(e)}", 3)
            if c:
                icon, name, count = parseReward(reward, day + 1)
                logging(
                    f"{counter_str}{name} x{count} claimed successfully!", 4, icon)
            else:
                logging(
                    f"{counter_str}Failed to claim daily! {c['message']}", 2)
        else:
            if isinstance(check, (int, bool)):
                logging(f"{counter_str}Already claimed")
            else:
                logging(counter_str + check)


if __name__ == "__main__":
    main()
